# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import string

import scrapy

class News(scrapy.Item):
    # url: str
    # content: str: processed text (this shouldn't get written)
    # relevant: bool: is about athlete injudires
    link = scrapy.Field()
    content = scrapy.Field()
    relevant = scrapy.Field()
