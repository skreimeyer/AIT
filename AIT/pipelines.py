# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from sklearn import feature_extraction, naive_bayes


# vectorizer = feature_extraction.text.TfidfVectorizer(max_features=10000, ngram_range=(1,2))
# classifier = naive_bayes.MultinomialNB()
# y = dtf_train["y"]
# X_names = vectorizer.get_feature_names()
# p_value_limit = 0.95
# dtf_features = pd.DataFrame()
# for cat in np.unique(y):
#     chi2, p = feature_selection.chi2(X_train, y==cat)
#     dtf_features = dtf_features.append(pd.DataFrame(
#                    {"feature":X_names, "score":1-p, "y":cat}))
#     dtf_features = dtf_features.sort_values(["y","score"], 
#                     ascending=[True,False])
#     dtf_features = dtf_features[dtf_features["score"]>p_value_limit]
# X_names = dtf_features["feature"].unique().tolist()

class AitPipeline:
    def process_item(self, item, spider):
        return item
