import string

import scrapy

from bs4 import BeautifulSoup

from AIT.items import News

from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import porter, wordnet

# Avoid instantiating these every time
STOPS = set(stopwords.words('english'))
LEMMER = wordnet.WordNetLemmatizer()
STEMMER = porter.PorterStemmer()

def standardize(source: str) -> str:
    # remove punctuation, capitalization and trailing whitespace
    # tokenize and remove stopwords. stem and lemmatize each word
    t = str.maketrans('','',string.punctuation)
    simplified = source.translate(t).lower().strip()
    words = [w for w in word_tokenize(simplified) if w not in STOPS]
    words = [LEMMER.lemmatize(STEMMER.stem(w)) for w in words]
    return " ".join(words)

class PositiveSpider(scrapy.Spider):
    name = 'positive'
    start_urls = ['https://www.campfire.wiki/doku.php?id=covid-19_vaccines:injuries:athletes']

    def parse(self, response: scrapy.http.Response):
        if "campfire" not in response.url:
            link = response.url
            soup = BeautifulSoup(response.text,'lxml')
            text = soup.get_text(strip=True)
            content = standardize(text)
            relevant = True
            yield News(link=link,content=content,relevant=relevant)
        for footnote in response.xpath('//div[@class="fn"]/div/a[@class="urlextern"]/@href').getall():
            yield response.follow(footnote,self.parse)